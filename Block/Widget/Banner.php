<?php
namespace Swissclinic\Widgets\Block\Widget;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;

class Banner extends Template implements BlockInterface
{

    public function __construct(
        Template\Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
    }

    protected $_template = "widget/banner.phtml";

    private function buildSrcSet () {
        $data = $this->getData();

        $srcSet = array();

        $srcSet['desktop']['standard']['1x'] = isset($data['dektop_img_url']) ? $data['dektop_img_url'] : '';
        $srcSet['desktop']['standard']['2x'] = isset($data['dektop_img_url_2x']) ? ", {$data['dektop_img_url_2x']} 2x" : '';

        $srcSet['tablet']['standard']['1x'] = isset($data['tablet_img_url']) ? $data['tablet_img_url'] : '';
        $srcSet['tablet']['standard']['2x'] = isset($data['tablet_img_url_2x']) ? ", {$data['tablet_img_url_2x']} 2x" : '';

        $srcSet['mobile']['standard']['1x'] = isset($data['mobile_img_url']) ? $data['mobile_img_url'] : '';
        $srcSet['mobile']['standard']['2x'] = isset($data['mobile_img_url_2x']) ? ", {$data['mobile_img_url_2x']} 2x" : '';

        $srcSet['desktop']['webp']['1x'] = isset($data['webp_dektop_img_url']) ? $data['webp_dektop_img_url'] : '';
        $srcSet['desktop']['webp']['2x'] = isset($data['webp_dektop_img_url_2x']) ? ", {$data['webp_dektop_img_url_2x']} 2x" : '';

        $srcSet['tablet']['webp']['1x'] = isset($data['webp_tablet_img_url']) ? $data['webp_tablet_img_url'] : '';
        $srcSet['tablet']['webp']['2x'] = isset($data['webp_tablet_img_url_2x']) ? ", {$data['webp_tablet_img_url_2x']} 2x" : '';

        $srcSet['mobile']['webp']['1x'] = isset($data['webp_mobile_img_url']) ? $data['webp_mobile_img_url'] : '';
        $srcSet['mobile']['webp']['2x'] = isset($data['webp_mobile_img_url_2x']) ? ", {$data['webp_mobile_img_url_2x']} 2x" : '';

        return $srcSet;
    }

    private function buildSource () {
        $data = $this->getData();
        $html = '';
        $altText = isset($data['alt_text']) ? $data['alt_text'] : '';

        $srcSet = $this->buildSrcSet();

        if (isset($data['webp_dektop_img_url'])) {
            $desktopsrcset = join('', $srcSet['desktop']['webp']);
            $html .= "<source type=\"image/webp\" media=\"(min-width: 1280px)\" srcset=\"{$desktopsrcset}\">";
        }

        if (isset($data['webp_tablet_img_url'])) {
            $tabletsrcset = join('', $srcSet['tablet']['webp']);
            $html .= "<source type=\"image/webp\" media=\"(min-width: 768px)\" srcset=\"{$tabletsrcset}\">";

        }
        if (isset($data['webp_mobile_img_url'])) {
            $mobilesrcset = join('', $srcSet['mobile']['webp']);
            $html .= "<source type=\"image/webp\" srcset=\"{$mobilesrcset}\">";
        }

        if (isset($data['dektop_img_url'])) {
            $desktopsrcset = join('', $srcSet['desktop']['standard']);
            $html .= "<source media=\"(min-width: 1280px)\" srcset=\"{$desktopsrcset}\">";
        }

        if (isset($data['tablet_img_url'])) {
            $tabletsrcset = join('', $srcSet['tablet']['standard']);
            $html .= "<source media=\"(min-width: 768px)\" srcset=\"{$tabletsrcset}\">";

        }
        if (isset($data['mobile_img_url'])) {
            $mobilesrcset = join('', $srcSet['mobile']['standard']);
            $html .= "<img src=\"{$data['mobile_img_url']}\" srcset=\"{$mobilesrcset}\" alt=\"{$altText}\">";
        }

        return $html;
    }

    public function buildHtml () {
        $data = $this->getData();

        $html = '';
        if(isset($data['href'])) {
            $html .= "<a href=\"{$data['href']}\">";
        }
        $html .= "<div class=\"{$data['banner_type']}\"><picture>";
        $html .= $this->buildSource();
        $html .= "</picture></div>";

        if(isset($data['href'])) {
            $html .= '</a>';
        }
        return $html;
    }
 }