<?php
namespace Swissclinic\Widgets\Block\Widget;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;

class TrustBanner extends Template implements BlockInterface
{
    protected $_template = "widget/trust-banner.phtml";
}